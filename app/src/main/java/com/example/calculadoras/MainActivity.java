package com.example.calculadoras;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = findViewById(R.id.btnGos);
        button.setOnClickListener(this);

        Button button2 = findViewById(R.id.btnDivisa);
        button2.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if(view.getId()==R.id.btnGos){
            Intent intent = new Intent (this, MainEdatGos.class);
            startActivity(intent);
        }if(view.getId()==R.id.btnDivisa){
            Intent intent2 = new Intent (this, MainDivisa.class);
            startActivity(intent2);
        }

    }
}
