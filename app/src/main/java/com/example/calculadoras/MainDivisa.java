package com.example.calculadoras;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainDivisa extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_divisa);
    }

    public void pushButton(View view) {

        TextView tvMayor;
        tvMayor = findViewById(R.id.tvDResult);

        double euros;
        EditText etEMessageDivisa = findViewById (R.id.etDMessage);
        euros = Double.parseDouble(etEMessageDivisa.getText().toString());

        double canvi = 0.86;

        double dolars = euros * canvi;

        tvMayor.setText(dolars+"");

    }
}
