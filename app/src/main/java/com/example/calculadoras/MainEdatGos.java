package com.example.calculadoras;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainEdatGos extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_edat_gos);
    }

    public void pushButton(View view) {

        TextView tvMayor;
        tvMayor = findViewById(R.id.tvEResult);

        double anys;
        EditText etEMessageAnys = findViewById (R.id.etEMessageAnys);
        anys = Double.parseDouble(etEMessageAnys.getText().toString());

        double mesos;
        EditText etEMessageMesos = findViewById (R.id.etEMessageMesos);
        mesos = Double.parseDouble(etEMessageMesos.getText().toString());

        double anysPersona = anys * 7;
        double mesosP = (mesos/12);
        double mesosPersona = mesosP * 7;
        double edatTotal = anysPersona + mesosPersona;

        tvMayor.setText(edatTotal+"");

    }


}
